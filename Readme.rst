================
Coderwall Badges
================

It is a chrome extension to show the badges earned at coderwall. Just fill in the GitHub username and check out!

.. img:: http://i.imgur.com/WT46F.png

Author
======

Vinit Kumar (@vinitcool76)
